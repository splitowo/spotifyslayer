package com.splitowo.spotifyslayer

import android.content.Context
import android.content.pm.ApplicationInfo
import android.media.AudioManager
import android.service.notification.NotificationListenerService
import android.service.notification.StatusBarNotification
import android.util.Log
import android.widget.Toast
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

import java.util.HashSet

class SpotifyNotificationListener : NotificationListenerService() {

    override fun onNotificationPosted(sbn: StatusBarNotification) {
        val notificationApplicationInfo = sbn.notification.extras.get("android.appInfo") as ApplicationInfo?
                ?: return
        val notificationAppOwner = notificationApplicationInfo.packageName
        if (OFFENDER_APP_PACKAGE_NAME == notificationAppOwner) {
            val notificationTitle = sbn.notification.extras.get("android.title") ?: return

            adjustMusicStream(OFFENDER_NOTIFICATION_TITLES.contains(notificationTitle.toString()))
        }
    }

    private fun adjustMusicStream(mute: Boolean) {
        val audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager?
        if (audioManager == null) {
            Log.i(applicationInfo.name, application.applicationInfo.name + getString(R.string.audio_service_request_error))
            return
        }

        val streamIsMuted = audioManager.isStreamMute(AudioManager.STREAM_MUSIC)
        if (streamIsMuted == mute) return

        Toast.makeText(this, applicationContext.getString(if (mute) R.string.sound_mute else R.string.sound_restore), Toast.LENGTH_SHORT).show()
        GlobalScope.launch {
            /*
             * Spotify notification is updated just before actual audio.
             * Delay the action to sync with audio, so the user doesn't hear that last bit of ad
             */
            delay(1400L)
            audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, if (mute) AudioManager.ADJUST_MUTE else AudioManager.ADJUST_UNMUTE, 0)
        }
    }

    companion object {
        private const val OFFENDER_APP_PACKAGE_NAME = "com.spotify.music"
        private val OFFENDER_NOTIFICATION_TITLES: MutableSet<String>
        init {
            OFFENDER_NOTIFICATION_TITLES = HashSet()
            OFFENDER_NOTIFICATION_TITLES.add("Advertisement")
            OFFENDER_NOTIFICATION_TITLES.add("Spotify")
        }
    }
}
