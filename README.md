Spotify Slayer
==============

Mutes Spotify advertisements.

This app needs an access to the notification area in order to work. Look up "Notification access" under your device settings to grant the permission.